#!/usr/bin/python

import time
import bluetooth
import RPi.GPIO as GPIO
from bt_rssi import BluetoothRSSI
from multiprocessing.pool import ThreadPool
import logging
logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)


GPIO.setmode(GPIO.BCM)
GPIO_PIR = 7
RELAY  = 23

GPIO.setup(GPIO_PIR,GPIO.IN)
#GPIO.setup(RELAY, GPIO.OUT, initial=GPIO.PUD_DOWN)
GPIO.setup(RELAY, GPIO.OUT)
GPIO.output(RELAY,1)
GPIO.setwarnings(False)

Current_State = 0
Previous_State = 0

#BT addresses
gregory = 'f4:60:e2:dc:47:35'
stas = '94:7B:E7:95:9A:FE'
oleg = 'F8:C3:9E:44:19:28'
sasha = '5C:09:47:A1:F1:31'
oleg_watches = '22:22:0e:c0:d0:94'
devices = [stas,gregory,oleg,oleg_watches,sasha]
logging.info('start')

def getRSSI(addr):
  rssi = BluetoothRSSI(addr=addr).request_rssi()

  logging.info('device MAC:%s', addr)
  logging.info('device RSSI:%s', rssi)

  return rssi

def openDoor():
    pool = ThreadPool(len(devices))
    res = pool.map(getRSSI, devices)

    pool.close()
    pool.join()

    if 0 in res:        
        # Set relay to 1 |Open door
        GPIO.output(RELAY,0)
        logging.info('Door opened')
        # Wait while door closing
        time.sleep(5)
        # Set relay 0
        GPIO.output(RELAY,1)
        logging.info('Door locked')    

    
try:

  while True :
    # Read PIR state
    Current_State = GPIO.input(GPIO_PIR)
    if Current_State==1 and Previous_State==0:
      # PIR is triggered
      logging.info('Motion detected!')
      # Delay before call function. If someone goes out will have some time to go out of range
      time.sleep(1)

      openDoor()

      isDone = GPIO.input(RELAY)

      if isDone==1:
        time.sleep(5)
        # Record previous state
        Previous_State=1

    elif Current_State==0 and Previous_State==1:
      # REED has returned to ready state
      logging.info('Ready for new detection')
      Previous_State=0
    # Wait for 10 milliseconds
    time.sleep(0.01)
except KeyboardInterrupt:
  print("Quit")
# Reset GPIO settings
  GPIO.cleanup()